using COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace BIZ.Managers
{
	public class SensorManager : GenericManager<Sensor>
	{
		public SensorManager(string nombreDelProcedimiento, AbstractValidator<Sensor> validator, string nombreDeCampoId) : base(nombreDelProcedimiento, validator, nombreDeCampoId)
		{
		}
	}
}
