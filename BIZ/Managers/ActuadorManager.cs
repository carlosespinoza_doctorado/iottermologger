using COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace BIZ.Managers
{
	public class ActuadorManager : GenericManager<Actuador>
	{
		public ActuadorManager(string nombreDelProcedimiento, AbstractValidator<Actuador> validator, string nombreDeCampoId) : base(nombreDelProcedimiento, validator, nombreDeCampoId)
		{
		}
	}
}
