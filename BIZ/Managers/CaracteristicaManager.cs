using COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace BIZ.Managers
{
	public class CaracteristicaManager : GenericManager<Caracteristica>
	{
		public CaracteristicaManager(string nombreDelProcedimiento, AbstractValidator<Caracteristica> validator, string nombreDeCampoId) : base(nombreDelProcedimiento, validator, nombreDeCampoId)
		{
		}
	}
}
