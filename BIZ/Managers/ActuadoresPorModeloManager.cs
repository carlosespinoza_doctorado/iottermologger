using COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace BIZ.Managers
{
	public class ActuadoresPorModeloManager : GenericManager<ActuadoresPorModelo>
	{
		public ActuadoresPorModeloManager(string nombreDelProcedimiento, AbstractValidator<ActuadoresPorModelo> validator, string nombreDeCampoId) : base(nombreDelProcedimiento, validator, nombreDeCampoId)
		{
		}
	}
}
