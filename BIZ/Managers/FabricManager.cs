﻿using BIZ.Managers;
using COMMON.Validadores;

namespace BIZ.Managers
{
    public static class FabricManager
    {
        public static ActuadorManager ActuadorManager() => new ActuadorManager("Proc_Actuador_CRUD", new ActuadorValidator(), "IdActuador");
        public static ActuadoresPorModeloManager ActuadoresPorModeloManager() => new ActuadoresPorModeloManager("Proc_ActuadoresPorModelo_CRUD", new ActuadoresPorModeloValidator(), "Id");
        public static AlertaManager AlertaManager() => new AlertaManager("Proc_Alerta_CRUD", new AlertaValidator(), "IdAlerta");
        public static AlertaEnviadaManager AlertaEnviadaManager() => new AlertaEnviadaManager("Proc_AlertaEnviada_CRUD", new AlertaEnviadaValidator(), "IdAlertaEnviada");
        public static AreaManager AreaManager() => new AreaManager("Proc_Area_CRUD", new AreaValidator(), "IdArea");
        public static CaracteristicaManager CaracteristicaManager() => new CaracteristicaManager("Proc_Caracteristica_CRUD", new CaracteristicaValidator(), "IdCaracteristica");
        public static CaracteristicasDeModeloManager CaracteristicasDeModeloManager() => new CaracteristicasDeModeloManager("Proc_CaracteristicasDeModelo_CRUD", new CaracteristicasDeModeloValidator(), "Id");
        public static DispositivoManager DispositivoManager() => new DispositivoManager("Proc_Dispositivo_CRUD", new DispositivoValidator(), "IdDispositivo");
        public static InstitucionManager InstitucionManager() => new InstitucionManager("Proc_Institucion_CRUD", new InstitucionValidator(), "IdInstitucion");
        public static MedicionManager MedicionManager() => new MedicionManager("Proc_Medicion_CRUD", new MedicionValidator(), "IdMedicion");
        public static ModeloManager ModeloManager() => new ModeloManager("Proc_Modelo_CRUD", new ModeloValidator(), "IdModelo");
        public static ReponsablesDeRegistroManager ReponsablesDeRegistroManager() => new ReponsablesDeRegistroManager("Proc_ReponsablesDeRegistro_CRUD", new ReponsablesDeRegistroValidator(), "IdResponsable");
        public static SensorManager SensorManager() => new SensorManager("Proc_Sensor_CRUD", new SensorValidator(), "IdSensor");
        public static SensoresPorModeloManager SensoresPorModeloManager() => new SensoresPorModeloManager("Proc_SensoresPorModelo_CRUD", new SensoresPorModeloValidator(), "Id");
        public static UsuarioManager UsuarioManager() => new UsuarioManager("Proc_Usuario_CRUD", new UsuarioValidator(), "IdUsuario");
        public static ValoresNormalesManager ValoresNormalesManager() => new ValoresNormalesManager("Proc_ValoresNormales_CRUD", new ValoresNormalesValidator(), "IdValorNormal");
    }
}
