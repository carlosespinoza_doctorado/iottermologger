using COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace BIZ.Managers
{
	public class InstitucionManager : GenericManager<Institucion>
	{
		public InstitucionManager(string nombreDelProcedimiento, AbstractValidator<Institucion> validator, string nombreDeCampoId) : base(nombreDelProcedimiento, validator, nombreDeCampoId)
		{
		}
	}
}
