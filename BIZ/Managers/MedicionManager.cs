using COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace BIZ.Managers
{
	public class MedicionManager : GenericManager<Medicion>
	{
		public MedicionManager(string nombreDelProcedimiento, AbstractValidator<Medicion> validator, string nombreDeCampoId) : base(nombreDelProcedimiento, validator, nombreDeCampoId)
		{
		}
	}
}
