using COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace BIZ.Managers
{
	public class ModeloManager : GenericManager<Modelo>
	{
		public ModeloManager(string nombreDelProcedimiento, AbstractValidator<Modelo> validator, string nombreDeCampoId) : base(nombreDelProcedimiento, validator, nombreDeCampoId)
		{
		}
	}
}
