﻿using FluentValidation;
using FluentValidation.Results;
using System.Reflection;

namespace BIZ.Managers
{
    public class GenericManager<T> where T : class
    {
        protected Procedimiento procedimiento;
        private AbstractValidator<T> validador;
        public string CampoId { get; private set; }
        public GenericManager(string nombreProcedimiento, AbstractValidator<T> validador, string campoId)
        {
            Error = "";
            procedimiento = new Procedimiento(nombreProcedimiento);
            this.validador = validador;
            CampoId = campoId;
        }


        public string Error { get; private set; }

        public List<T> ObtenerTodos
        {

            get
            {
                procedimiento.NuevaPeticion(1);
                try
                {
                    if (procedimiento.Ejecutar())
                    {
                        Error = "";
                        return procedimiento.ObtenerRespuestaComoLista<T>();
                    }
                    else
                    {
                        Error = procedimiento.Error;
                        return null;
                    }
                }
                catch (Exception ex)
                {
                    Error = ex.Message;
                    return null;
                }
            }
        }



        public IEnumerable<M> EjecutarVista<M>(int accion) where M : class
        {
            try
            {
                procedimiento.NuevaPeticion(accion);
                if (procedimiento.Ejecutar())
                {
                    Error = "";
                    return procedimiento.ObtenerRespuestaComoLista<M>();
                }
                else
                {
                    Error = procedimiento.Respuesta.Error;
                    return null;
                }
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return null;
            }
        }



        public bool Eliminar(T entidad)
        {
            procedimiento.NuevaPeticion(6);
            procedimiento.AgregarParametro(CampoId, ObtenerValor(CampoId, entidad), System.Data.SqlDbType.Int);
            try
            {
                if (procedimiento.Ejecutar())
                {
                    Error = "";
                    return procedimiento.Respuesta.Exito;
                }
                else
                {
                    Error = procedimiento.Error;
                    return false;
                }
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return false;
            }
        }

        public T Insertar(T entidad)
        {
            ValidationResult validacion = validador.Validate(entidad);
            if (validacion.IsValid)
            {
                procedimiento.NuevaPeticion(4);
                procedimiento.AgregarParametroJSON(entidad);
                try
                {
                    if (procedimiento.Ejecutar())
                    {
                        Error = "";
                        return procedimiento.ObtenerRespuestaComoObjeto<T>();
                    }
                    else
                    {
                        Error = procedimiento.Error;
                        return null;
                    }
                }
                catch (Exception ex)
                {
                    Error = ex.Message;
                    return null;
                }
            }
            else
            {
                Error = "Error: ";
                foreach (var item in validacion.Errors)
                {
                    Error += item.ErrorMessage + ". ";
                }
                return null;
            }



        }

        public T Modificar(T entidad)
        {
            ValidationResult validacion = validador.Validate(entidad);
            if (validacion.IsValid)
            {
                procedimiento.NuevaPeticion(5);
                procedimiento.AgregarParametroJSON(entidad);
                procedimiento.AgregarParametro(CampoId, ObtenerValor(CampoId, entidad), System.Data.SqlDbType.Int);
                try
                {
                    if (procedimiento.Ejecutar())
                    {
                        Error = "";
                        return procedimiento.ObtenerRespuestaComoObjeto<T>();
                    }
                    else
                    {
                        Error = procedimiento.Error;
                        return null;
                    }
                }
                catch (Exception ex)
                {
                    Error = ex.Message;
                    return null;
                }
            }
            else
            {
                Error = "Error: ";
                foreach (var item in validacion.Errors)
                {
                    Error += item.ErrorMessage + ". ";
                }
                return null;
            }
        }

        private string ObtenerValor(string campoId, T entidad)
        {
            var campos = typeof(T).GetProperties();
            Type ttype = typeof(T);
            PropertyInfo property = ttype.GetProperty(campoId);
            return property.GetValue(entidad).ToString();
        }

        public T ObtenerPorId(int id)
        {
            return ObtenerPorId(id.ToString());
        }

        public T ObtenerPorId(string id)
        {
            procedimiento.NuevaPeticion(2);
            procedimiento.AgregarParametro(CampoId, id, System.Data.SqlDbType.Int);
            try
            {
                if (procedimiento.Ejecutar())
                {
                    Error = "";
                    return procedimiento.ObtenerRespuestaComoObjeto<T>();
                }
                else
                {
                    Error = procedimiento.Error;
                    return null;
                }
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return null;
            }
        }
    }
}

