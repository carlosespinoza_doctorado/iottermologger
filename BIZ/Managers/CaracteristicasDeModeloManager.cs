using COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace BIZ.Managers
{
	public class CaracteristicasDeModeloManager : GenericManager<CaracteristicasDeModelo>
	{
		public CaracteristicasDeModeloManager(string nombreDelProcedimiento, AbstractValidator<CaracteristicasDeModelo> validator, string nombreDeCampoId) : base(nombreDelProcedimiento, validator, nombreDeCampoId)
		{
		}
	}
}
