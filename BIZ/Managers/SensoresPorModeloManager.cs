using COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace BIZ.Managers
{
	public class SensoresPorModeloManager : GenericManager<SensoresPorModelo>
	{
		public SensoresPorModeloManager(string nombreDelProcedimiento, AbstractValidator<SensoresPorModelo> validator, string nombreDeCampoId) : base(nombreDelProcedimiento, validator, nombreDeCampoId)
		{
		}
	}
}
