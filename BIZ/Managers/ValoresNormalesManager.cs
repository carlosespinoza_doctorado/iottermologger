using COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace BIZ.Managers
{
	public class ValoresNormalesManager : GenericManager<ValoresNormales>
	{
		public ValoresNormalesManager(string nombreDelProcedimiento, AbstractValidator<ValoresNormales> validator, string nombreDeCampoId) : base(nombreDelProcedimiento, validator, nombreDeCampoId)
		{
		}
	}
}
