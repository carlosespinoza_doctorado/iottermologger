using COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace BIZ.Managers
{
	public class AlertaManager : GenericManager<Alerta>
	{
		public AlertaManager(string nombreDelProcedimiento, AbstractValidator<Alerta> validator, string nombreDeCampoId) : base(nombreDelProcedimiento, validator, nombreDeCampoId)
		{
		}
	}
}
