using COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace BIZ.Managers
{
	public class UsuarioManager : GenericManager<Usuario>
	{
		public UsuarioManager(string nombreDelProcedimiento, AbstractValidator<Usuario> validator, string nombreDeCampoId) : base(nombreDelProcedimiento, validator, nombreDeCampoId)
		{
		}
	}
}
