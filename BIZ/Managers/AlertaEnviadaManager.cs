using COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace BIZ.Managers
{
	public class AlertaEnviadaManager : GenericManager<AlertaEnviada>
	{
		public AlertaEnviadaManager(string nombreDelProcedimiento, AbstractValidator<AlertaEnviada> validator, string nombreDeCampoId) : base(nombreDelProcedimiento, validator, nombreDeCampoId)
		{
		}
	}
}
