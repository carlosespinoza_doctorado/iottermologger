using COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace BIZ.Managers
{
	public class ReponsablesDeRegistroManager : GenericManager<ReponsablesDeRegistro>
	{
		public ReponsablesDeRegistroManager(string nombreDelProcedimiento, AbstractValidator<ReponsablesDeRegistro> validator, string nombreDeCampoId) : base(nombreDelProcedimiento, validator, nombreDeCampoId)
		{
		}
	}
}
