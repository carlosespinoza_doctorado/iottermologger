using COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace BIZ.Managers
{
	public class DispositivoManager : GenericManager<Dispositivo>
	{
		public DispositivoManager(string nombreDelProcedimiento, AbstractValidator<Dispositivo> validator, string nombreDeCampoId) : base(nombreDelProcedimiento, validator, nombreDeCampoId)
		{
		}
	}
}
