﻿using WebApiModels;

namespace BIZ
{
    /// <summary>
    /// Configura los parámetros para el uso de la biblioteca
    /// </summary>
    public static class Params
    {
        /// <summary>
        /// Almacena el JWT para el uso de la API, EXCLUSIVO DE USO DE LA LIBRERIA 
        /// </summary>
        public static Token token;
        /// <summary>
        /// Almacena el usuario para el uso de la API
        /// </summary>
        public static string JWTUser = "TermoUser";
        /// <summary>
        /// Almacena el password para el uso de la API
        /// </summary>
        public static string JWTPassword = "7854qwerty78";
        /// <summary>
        /// Almacena la URI de la API
        /// </summary>
        public static string ApiBaseAddress = @"http://200.79.179.167:49158/";
    }
}
