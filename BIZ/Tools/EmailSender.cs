﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BIZ.Tools
{
    public class EmailSender
    {
        string SMTP, SMTPUsuario, SMTPPassword;
        int SMTPPort;
        public EmailSender(string SmtpServer, int SmtpPort, string emailOrigen, string emailPassword)
        {
            SMTP = SmtpServer;
            SMTPPort = SmtpPort;
            SMTPUsuario = emailOrigen;
            SMTPPassword = emailPassword;
        }
        public void EnviarCorreo(string destinos, string titulo, string cuerpo)
        {
            var client = new SmtpClient(SMTP, SMTPPort)
            {
                Credentials = new NetworkCredential(SMTPUsuario, SMTPPassword),
                EnableSsl = true
            };
            client.Send(SMTPUsuario, destinos, titulo, cuerpo);
        }
    }
}
