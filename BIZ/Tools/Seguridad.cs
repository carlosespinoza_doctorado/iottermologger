﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BIZ.Tools
{
    public static class Seguridad
    {
        /// <summary>
        /// Verifica que el password cumpla con la política de seguridad: Entre 8 y 15 caracteres, Mayúsculas, Caracteres especiales, números y minúsculas
        /// </summary>
        /// <param name="password">cadena con la contraseña a validar</param>
        /// <returns>Bool que indica si la contraseña es valida</returns>
        public static bool ValidarPassword(string password)
        {
            Regex regex = new Regex(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?=;&#.$($)$-$_])[A-Za-z\d$@$!%*?=;&#.$($)$-$_]{8,15}$");
            return regex.IsMatch(password);
        }
        /// <summary>
        /// Encripta una cadena, útil para contraseñas, mediante un algoritmo asimétrico SHA256
        /// </summary>
        /// <param name="str">Cadena a encriptar</param>
        /// <returns>Cadena encriptada</returns>
        public static string EncriptarCadenaSHA256(string str)
        {
            SHA256 sha256 = SHA256.Create();
            ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] stream = null;
            StringBuilder sb = new StringBuilder();
            stream = sha256.ComputeHash(encoding.GetBytes(str));
            for (int i = 0; i < stream.Length; i++) sb.AppendFormat("{0:x2}", stream[i]);
            return sb.ToString();
        }
    }
}
