﻿using WebApiModels;
using System.Text;
using Newtonsoft.Json;

namespace BIZ
{
    /// <summary>
    /// Construye y ejecuta la llamada a un procedimiento almacenado, SOLO DEBE SER USADA EN LA CAPA DE NEGOCIO.
    /// </summary>
    public class Procedimiento
    {

        /// <summary>
        /// Obtiene el error, si es que lo hay, generado al consumir el API, puede ser un error de procesamiento o el error que arroja la API directamente.
        /// </summary>
        public string Error { get; private set; }
        private HttpClient client;
        private string uriApiProcedimiento;

        #region LlamadaHTTP
        private Respuesta EjecutarProcedimiento(Peticion peticion) => EjecutarProcedimientoAsync(peticion).Result;
        private async Task<Respuesta> EjecutarProcedimientoAsync(Peticion peticion)
        {
            try
            {
                var token = await ObtenerToken().ConfigureAwait(false);
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

                var c = JsonConvert.SerializeObject(peticion);
                var body = new StringContent(c, Encoding.UTF8, "application/json");
                HttpResponseMessage reponse = await client.PostAsync($"api/Procedimiento", body).ConfigureAwait(false);
                var content = await reponse.Content.ReadAsStringAsync().ConfigureAwait(false);
                var respuesta = JsonConvert.DeserializeObject<Respuesta>(content);
                if (reponse.IsSuccessStatusCode)
                {
                    if (respuesta.Exito)
                    {
                        Error = "";
                        return respuesta;
                    }
                    else
                    {
                        Error = respuesta.Error;
                        return respuesta;
                    }
                }
                else
                {
                    Error = respuesta.Error;
                    return respuesta;
                }
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return null;
            }

        }

        private async Task<string> ObtenerToken()
        {
            if (Params.token != null)
            {
                if (Params.token.Expira < DateTime.UtcNow)
                {
                    Params.token = null;
                    return ObtenerToken().Result;
                }
                else
                {
                    return Params.token.TokenString;
                }
            }
            else
            {
                try
                {
                    Login login = new Login()
                    {
                        NombreDeUsuario = Params.JWTUser,
                        Password = Params.JWTPassword
                    };
                    HttpContent content = new StringContent(JsonConvert.SerializeObject(login), Encoding.UTF8, "application/json");
                    HttpResponseMessage response = await client.PostAsync("api/Procedimiento/Auth", content).ConfigureAwait(false);

                    if (response.IsSuccessStatusCode)
                    {
                        var respuesta = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                        var token = JsonConvert.DeserializeObject<Token>(respuesta);
                        Params.token = token;
                        return token.TokenString;
                    }
                    else
                    {
                        throw new Exception("Credenciales de uso de API invalidas...");
                    }
                }
                catch (Exception ex)
                {
                    Error = ex.Message;
                    return null;
                }
            }
        }
        #endregion

        /// <summary>
        /// Obtiene la respuesta de la ejecución del procedimiento y la convierte a una lista de objetos
        /// </summary>
        /// <typeparam name="T">Tipo de objetos de la lista a obtener</typeparam>
        /// <returns>Lista de objetos</returns>
        public List<T> ObtenerRespuestaComoLista<T>() where T : class
        {
            return Respuesta.ObtenerRespuestaComoLista<T>();
        }
        /// <summary>
        /// Obtiene la respuesta de la ejecución del procedimiento y la convierte en un solo objeto, Útil cuando el resultado del procedimiento es un solo registro.
        /// </summary>
        /// <typeparam name="T">Tipo de objeto a obtener</typeparam>
        /// <returns>Objeto resultante</returns>
        public T ObtenerRespuestaComoObjeto<T>() where T : class
        {
            return ObtenerRespuestaComoLista<T>().SingleOrDefault();
        }

        /// <summary>
        /// Ejecuta la petición actual.
        /// </summary>
        /// <returns>Si el procedimiento tubo éxito al ejecutarse</returns>
        public bool Ejecutar()
        {
            Respuesta = EjecutarProcedimiento(Peticion);
            return Respuesta != null ? Respuesta.Exito : false;
        }

        /// <summary>
        /// Obtiene la respuesta del procedimiento, después de ser ejecutado
        /// </summary>
        public Respuesta Respuesta { get; private set; }
        /// <summary>
        /// Obtiene la petición del procedimiento a ejecutar
        /// </summary>
        public Peticion Peticion { get; private set; }
        /// <summary>
        /// Obtiene el nombre del procedimiento a Ejecutar
        /// </summary>
        public string NombreDelProcedimiento { get; private set; }
        /// <summary>
        /// Crea una instancia para poder ejecutar un procedimiento almacenado
        /// </summary>
        /// <param name="nombreDelProcedimiento">Nombre del procedimiento a ejecutar</param>
        public Procedimiento(string nombreDelProcedimiento)
        {
            NombreDelProcedimiento = nombreDelProcedimiento;
            client = new HttpClient();
            client.BaseAddress = new Uri(Params.ApiBaseAddress);
            uriApiProcedimiento = "api/Procedimiento/";
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            Respuesta = new Respuesta();
            Peticion = new Peticion();
            // NuevaPeticion(1);
        }

        /// <summary>
        /// Agrega un parámetro a la petición actual
        /// </summary>
        /// <param name="nombre">Nombre del Parámetro</param>
        /// <param name="valor">Valor del parámetro</param>
        /// <param name="tipo">Tipo del parámetro</param>
        public void AgregarParametro(string nombre, string valor, System.Data.SqlDbType tipo)
        {
            Peticion.Parametros.Add(new Parametro(nombre, valor, tipo));
        }
        /// <summary>
        /// Agrega un parámetro de tipo JSON (Internamente se convierte a JSON)
        /// </summary>
        /// <param name="objeto">Objeto a convertir en JSON</param>
        public void AgregarParametroJSON(object objeto)
        {
            Peticion.Parametros.Add(new Parametro("Json", JsonConvert.SerializeObject(objeto), System.Data.SqlDbType.NVarChar));
        }

        /// <summary>
        /// Agrega un parámetro de salida a la petición actual
        /// </summary>
        /// <param name="nombre"></param>
        /// <param name="valor"></param>
        /// <param name="tipo"></param>
        public void AgregarParametroDeSalida(string nombre, string valor, System.Data.SqlDbType tipo)
        {
            Peticion.ParametrosOut.Add(new Parametro(nombre, valor, tipo));
        }

        public void NuevaPeticion(int accion)
        {
            Peticion = new Peticion(NombreDelProcedimiento);
            Peticion.AgregaParametro("Accion", accion.ToString(), System.Data.SqlDbType.Int);
        }

    }
}

