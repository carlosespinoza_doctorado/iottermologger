using COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace COMMON.Validadores
{
	public class ValoresNormalesValidator : AbstractValidator<ValoresNormales>
	{
		public ValoresNormalesValidator()
		{
			RuleFor(V => V.Min).NotEmpty();
			RuleFor(V => V.Max).NotEmpty();
		}
	}
}
