using COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace COMMON.Validadores
{
	public class CaracteristicaValidator : AbstractValidator<Caracteristica>
	{
		public CaracteristicaValidator()
		{
			RuleFor(C => C.Nombre).NotEmpty().MaximumLength(50);
			RuleFor(C => C.Descripcion).NotEmpty().MaximumLength(100);
		}
	}
}
