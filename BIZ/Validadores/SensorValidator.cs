using COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace COMMON.Validadores
{
	public class SensorValidator : AbstractValidator<Sensor>
	{
		public SensorValidator()
		{
			RuleFor(S => S.Nombre).NotEmpty().MaximumLength(50);
			RuleFor(S => S.Caracteristicas).NotEmpty().MaximumLength(500);
			RuleFor(S => S.UnidadDeMedida).NotEmpty().MaximumLength(50);
		}
	}
}
