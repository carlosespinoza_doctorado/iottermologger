using COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace COMMON.Validadores
{
	public class AreaValidator : AbstractValidator<Area>
	{
		public AreaValidator()
		{
			RuleFor(A => A.Nombre).NotEmpty().MaximumLength(50);
		}
	}
}
