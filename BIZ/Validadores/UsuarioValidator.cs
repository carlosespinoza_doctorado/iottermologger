using COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace COMMON.Validadores
{
	public class UsuarioValidator : AbstractValidator<Usuario>
	{
		public UsuarioValidator()
		{
			RuleFor(U => U.Email).NotEmpty().MaximumLength(250);
			RuleFor(U => U.Password).NotEmpty().MaximumLength(50);
			RuleFor(U => U.Nombre).NotEmpty().MaximumLength(50);
			RuleFor(U => U.Apellidos).NotEmpty().MaximumLength(50);
			RuleFor(U => U.RutaAvatar).NotEmpty().MaximumLength(100);
			RuleFor(U => U.WhatsApp).NotEmpty().MaximumLength(50);
		}
	}
}
