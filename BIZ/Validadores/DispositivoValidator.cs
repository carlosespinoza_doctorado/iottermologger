using COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace COMMON.Validadores
{
	public class DispositivoValidator : AbstractValidator<Dispositivo>
	{
		public DispositivoValidator()
		{
			RuleFor(D => D.FechaColocacion).NotEmpty(); 
			RuleFor(D => D.Nombre).NotEmpty().MaximumLength(50);
			RuleFor(D => D.Notas).NotEmpty(); 
			RuleFor(D => D.Marca).NotEmpty().MaximumLength(50);
			RuleFor(D => D.Modelo).NotEmpty().MaximumLength(50);
			RuleFor(D => D.NoSerie).NotEmpty().MaximumLength(50);
		}	
	}
}
