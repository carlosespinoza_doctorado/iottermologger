using COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace COMMON.Validadores
{
	public class ActuadorValidator : AbstractValidator<Actuador>
	{
		public ActuadorValidator()
		{
			RuleFor(A => A.Nombre).NotEmpty().MaximumLength(50);
			RuleFor(A => A.Caracteristicas).NotEmpty().MaximumLength(500);
		}
	}
}
