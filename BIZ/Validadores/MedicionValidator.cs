using COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace COMMON.Validadores
{
	public class MedicionValidator : AbstractValidator<Medicion>
	{
		public MedicionValidator()
		{
			RuleFor(M => M.FechaHora).NotEmpty();
			RuleFor(M => M.IdDispositivo).NotEmpty().MaximumLength(50);
			RuleFor(M => M.Valor).NotEmpty();
		}
	}
}
