using COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace COMMON.Validadores
{
	public class ReponsablesDeRegistroValidator : AbstractValidator<ReponsablesDeRegistro>
	{
		public ReponsablesDeRegistroValidator()
		{
			RuleFor(R => R.NombreCompleto).NotEmpty().MaximumLength(250);
		}
	}
}
