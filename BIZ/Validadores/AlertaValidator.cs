using COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace COMMON.Validadores
{
	public class AlertaValidator : AbstractValidator<Alerta>
	{
		public AlertaValidator()
		{
			RuleFor(A => A.IntervaloInferior).NotEmpty();
			RuleFor(A => A.IntervaloSuperior).NotEmpty();
			RuleFor(A => A.Nombre).NotEmpty().MaximumLength(50);
		}
	}
}
