using COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace COMMON.Validadores
{
	public class AlertaEnviadaValidator : AbstractValidator<AlertaEnviada>
	{
		public AlertaEnviadaValidator()
		{
			RuleFor(A => A.FechaHora).NotEmpty();
		}
	}
}
