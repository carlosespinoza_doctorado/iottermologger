using COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace COMMON.Validadores
{
	public class InstitucionValidator : AbstractValidator<Institucion>
	{
		public InstitucionValidator()
		{
			RuleFor(I => I.Nombre).NotEmpty().MaximumLength(250);
			RuleFor(I => I.UrlImagen).NotEmpty().MaximumLength(250);
		}
	}
}
