using COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace COMMON.Validadores
{
	public class ModeloValidator : AbstractValidator<Modelo>
	{
		public ModeloValidator()
		{
			RuleFor(M => M.Nombre).NotEmpty().MaximumLength(50);
			RuleFor(M => M.FechaLanzamiento).NotEmpty();
			RuleFor(M => M.Notas).NotEmpty(); 
		}
	}
}
