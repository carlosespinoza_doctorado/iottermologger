using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Entidades
{
	public class AlertaEnviada
	{
		public long IdAlertaEnviada { get; set; }
		public int IdAlerta { get; set; }
		public long IdMedicion { get; set; }
		public DateTime FechaHora { get; set; }
	}
}
