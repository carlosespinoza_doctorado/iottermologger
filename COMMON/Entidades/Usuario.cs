using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Entidades
{
	public class Usuario
	{
		public string Email { get; set; }
		public string Password { get; set; }
		public string Nombre { get; set; }
		public string Apellidos { get; set; }
		public string? RutaAvatar { get; set; }
		public int IdInstitucion { get; set; }
		public bool EsAdministrador { get; set; }
		public string WhatsApp { get; set; }
		public int IdUsuario { get; set; }
	}
}
