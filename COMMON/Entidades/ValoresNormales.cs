using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Entidades
{
	public class ValoresNormales
	{
		public int IdValorNormal { get; set; }
		public int IdDispositivo { get; set; }
		public int IdSensor { get; set; }
		public float Min { get; set; }
		public float Max { get; set; }
	}
}
