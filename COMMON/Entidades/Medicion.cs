using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Entidades
{
	public class Medicion
	{
		public long IdMedicion { get; set; }
		public DateTime FechaHora { get; set; }
		public string IdDispositivo { get; set; }
		public float Valor { get; set; }
		public int IdSensor { get; set; }
	}
}
