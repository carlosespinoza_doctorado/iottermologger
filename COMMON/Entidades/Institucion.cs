using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Entidades
{
	public class Institucion
	{
		public int IdInstitucion { get; set; }
		public string Nombre { get; set; }
		public string Direccion { get; set; }
		public string? UrlImagen { get; set; }
	}
}
