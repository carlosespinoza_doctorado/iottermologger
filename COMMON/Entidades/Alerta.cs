using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Entidades
{
	public class Alerta
	{
		public int IdAlerta { get; set; }
		public int IdDispositivo { get; set; }
		public int Medicion { get; set; }
		public float IntervaloInferior { get; set; }
		public float IntervaloSuperior { get; set; }
		public string Nombre { get; set; }
		public int UsuarioNotificar { get; set; }
		public bool NotificarPorEmail { get; set; }
		public bool NotificarPorWhatsApp { get; set; }
	}
}
