using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Entidades
{
	public class Area
	{
		public int IdArea { get; set; }
		public int IdInstitucion { get; set; }
		public int UsuarioResponsable { get; set; }
		public string Nombre { get; set; }
	}
}
