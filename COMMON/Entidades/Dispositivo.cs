using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Entidades
{
	public class Dispositivo
	{
		public int IdDispositivo { get; set; }
		public int? IdArea { get; set; }
		public DateTime? FechaColocacion { get; set; }
		public string? Nombre { get; set; }
		public int? IdModelo { get; set; }
		public string? Notas { get; set; }
		public string? Marca { get; set; }
		public string? Modelo { get; set; }
		public string? NoSerie { get; set; }
	}
}
