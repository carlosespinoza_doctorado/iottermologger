using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Entidades
{
	public class SensoresPorModelo
	{
		public int Id { get; set; }
		public int IdSensor { get; set; }
		public int IdModelo { get; set; }
	}
}
