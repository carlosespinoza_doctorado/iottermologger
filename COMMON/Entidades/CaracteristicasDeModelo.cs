using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Entidades
{
	public class CaracteristicasDeModelo
	{
		public int Id { get; set; }
		public int IdModelo { get; set; }
		public int IdCaracteristica { get; set; }
	}
}
