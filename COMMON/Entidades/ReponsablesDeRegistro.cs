using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Entidades
{
	public class ReponsablesDeRegistro
	{
		public int IdResponsable { get; set; }
		public int IdArea { get; set; }
		public string NombreCompleto { get; set; }
	}
}
