using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Entidades
{
	public class Modelo
	{
		public int IdModelo { get; set; }
		public string Nombre { get; set; }
		public DateTime FechaLanzamiento { get; set; }
		public string? Notas { get; set; }
	}
}
