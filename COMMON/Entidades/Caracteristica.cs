using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Entidades
{
	public class Caracteristica
	{
		public int IdCaracteristica { get; set; }
		public string Nombre { get; set; }
		public string Descripcion { get; set; }
	}
}
