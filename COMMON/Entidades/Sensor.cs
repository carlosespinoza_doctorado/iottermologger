using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Entidades
{
	public class Sensor
	{
		public int IdSensor { get; set; }
		public string Nombre { get; set; }
		public string? Caracteristicas { get; set; }
		public string UnidadDeMedida { get; set; }
	}
}
