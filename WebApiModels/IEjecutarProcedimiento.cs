﻿

namespace WebApiModels
{
    /// <summary>
    /// Ejecuta un procedimiento almacenado dentro de una base de datos
    /// </summary>
    public interface IEjecutaProcedimiento
    {
        /// <summary>
        /// Obtiene el error, si es que lo hay, generado al consumir el API, puede ser un error de procesamiento o el error que arroja la API directamente.
        /// </summary>
        string Error { get; }
        /// <summary>
        /// Ejecuta un procedimiento almacenado
        /// </summary>
        /// <param name="peticion">Definición de la petición</param>
        /// <param name="tipoDeResultado">Tipo de objeto que entrega el resulta del procedimiento almacenado</param>
        /// <returns>Respuesta de la ejecución del procedimiento</returns>
        Respuesta EjecutarProcedimiento(Peticion peticion);
    }
}
