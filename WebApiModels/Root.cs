﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApiModels
{
    public class Root<T> where T:class
    {
        public List<T> Table { get; set; }
    }
}
