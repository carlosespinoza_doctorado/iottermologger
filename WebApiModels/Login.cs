﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiModels
{
    /// <summary>
    /// Define la credenciales para el uso de la API
    /// </summary>
    public class Login
    {
        /// <summary>
        /// Nombre de usuario
        /// </summary>
        public string NombreDeUsuario { get; set; }
        /// <summary>
        /// Contraseña
        /// </summary>
        public string Password { get; set; }
    }
}
