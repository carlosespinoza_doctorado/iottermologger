﻿
namespace WebApiModels
{
    /// <summary>
    /// Definición de base de datos
    /// </summary>
    public interface IDbRelacional
    {
        /// <summary>
        /// Error obtenido por la SGBD
        /// </summary>
        string Error { get; }
        /// <summary>
        /// Comando a ejecutar dentro de la BD
        /// </summary>
        /// <param name="command">SQL del comando</param>
        /// <returns>Número de filas afectadas</returns>
        int Comando(string command);
        /// <summary>
        /// Ejecuta una Consulta SQL
        /// </summary>
        /// <param name="consulta">Consulta SQL</param>
        /// <returns>DataReader con el resultado de la consulta</returns>
        object Consulta(string consulta);
        /// <summary>
        /// Elimina el objeto DB de la memoria
        /// </summary>
        void Dispose();
        /// <summary>
        /// Ejecuta un procedimiento almacenado en el SGBD
        /// </summary>
        /// <param name="peticion">Definición de la petición</param>
        /// <param name="json">Resultado de la petición (si aplica)</param>
        /// <param name="paramterosOut">Parámetros de salida del procedimiento almacenado (si aplica)</param>
        /// <returns></returns>
        bool EjecutarProcedimiento(Peticion peticion, out string json, out List<Parametro> paramterosOut);
        /// <summary>
        /// Permite el inicio de sesión en la API
        /// </summary>
        /// <param name="login">Credenciales</param>
        /// <returns>Si el usuario es valido</returns>
        bool Login(Login login);
    }
}
