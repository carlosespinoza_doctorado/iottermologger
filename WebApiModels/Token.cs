﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiModels
{ 
    /// <summary>
    /// Define el Token a ser usado en la API
    /// </summary>
    public class Token
    {
        /// <summary>
        /// Usuario que solicito el token
        /// </summary>
        public string Usuario { get; set; }
        /// <summary>
        /// Fecha y hora de expiración en UTC
        /// </summary>
        public DateTime Expira { get; set; }
        /// <summary>
        /// Token
        /// </summary>
        public string TokenString { get; set; }
    }
}
