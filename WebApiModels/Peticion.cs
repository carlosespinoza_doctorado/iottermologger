﻿using System.Data;

namespace WebApiModels
{
    /// <summary>
    /// Definición de la petición en la ejecución de un procedimiento almacenado.
    /// </summary>
    public class Peticion
    {
        /// <summary>
        /// Nombre del procedimiento a ejecutar.
        /// </summary>
        public string NombreProcedimiento { get; set; }
        /// <summary>
        /// Lista de parámetros del procedimiento almacenado.
        /// </summary>
        public List<Parametro> Parametros { get; set; }
        /// <summary>
        /// Lista de parámetros de salida para el procedimiento almacenado.
        /// </summary>
        public List<Parametro> ParametrosOut { get; set; }

        /// <summary>
        /// Crea una nueva petición
        /// </summary>
        /// <param name="nombreProcedimiento">Nombre del procedimiento a ejecutar</param>
        public Peticion(string nombreProcedimiento)
        {
            NombreProcedimiento=nombreProcedimiento;
            Parametros = new List<Parametro>();
            ParametrosOut = new List<Parametro>();
        }
        /// <summary>
        /// Crea una nueva petición
        /// </summary>
        public Peticion()
        {
            Parametros = new List<Parametro>();
            ParametrosOut=new List<Parametro>();
        }

        public void AgregaParametro(string nombre, string valor, SqlDbType tipo)
        {
            Parametros.Add(new Parametro(nombre,valor,tipo));
        }
    }
    /// <summary>
    /// Definición de un parámetro en un procedimiento almacenado.
    /// </summary>
    public class Parametro
    {
        /// <summary>
        /// Crea un parámetro para un procedimiento almacenado
        /// </summary>
        /// <param name="nombre">Nombre del parámetro</param>
        /// <param name="valor">Valor del parámetro</param>
        /// <param name="tipo">Tipo del parámetro</param>
        public Parametro(string nombre, string valor, SqlDbType tipo)
        {
            Nombre = nombre;
            Valor=valor;
            Tipo=tipo;
        }
        /// <summary>
        /// Nombre del parámetro.
        /// </summary>
        public string Nombre { get; set; }
        /// <summary>
        /// Valor del parámetro.
        /// </summary>
        public string Valor { get; set; }
        /// <summary>
        /// Tipo del parámetro SqlDbType
        /// </summary>
        public SqlDbType Tipo { get; set; }
    }
}
