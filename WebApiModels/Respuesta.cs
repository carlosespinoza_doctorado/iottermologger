﻿using Newtonsoft.Json;

namespace WebApiModels
{
    /// <summary>
    /// Respuesta de la API
    /// </summary>
    public class Respuesta
    {
        /// <summary>
        /// Indica si la llamada tuvo éxito
        /// </summary>
        public bool Exito { get; set; }
        /// <summary>
        /// Muestra el resultado de la llamada, dependiendo el tipo de llamada, esta puede ser un JSON con la o las tablas resultantes, un texto o un numero.
        /// </summary>
        public string Resultado { get; set; }
        /// <summary>
        /// En caso de error muestra el error obtenido, este puede ser por algún proceso de la API o venir directamente desde al SGBD.
        /// </summary>
        public string Error { get; set; }
        /// <summary>
        /// Lista de parámetros de salida resultados en el procedimiento almacenado (si aplica)
        /// </summary>
        public List<Parametro> ParametrosOut { get; set; }
        public Respuesta()
        {
            ParametrosOut= new List<Parametro>();
        }
        /// <summary>
        /// Convierte el resultado en una lista de objetos del tipo proporcionado
        /// </summary>
        /// <typeparam name="T">Tipo de datos al cual convertir la lista</typeparam>
        /// <returns>Lista de objetos de tipo proporcionado</returns>
        public List<T> ObtenerRespuestaComoLista<T>() where T : class
        {
            return JsonConvert.DeserializeObject<Root<T>>(Resultado).Table;
        }
    }
}
